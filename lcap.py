
import psutil
import sqlite3
import socket
import time
import subprocess
import json

# get network usage
#psutil.net_io_counters()

# network connections
#psutil.net_connections()

# variables
dns_resolved = {}
log_file = 'network_connections_'
now_date = str(int(time.time()))

# 0 - file descriptor
# 1 - connection type/family
# 2 - socket type
# 3 - local Address
#   0 - [string] ip Address
#   1 - [int] port number
# 4 - Remote Address
# 5 - status
# 6 - pid

def adjust_string(string, length):
	"""
	Adjusts string to specified length.
	:param string:
	:param length:
	:return:
	"""
	if length - len(string) > 0:
		for i in range(length - len(string)):
			string += ' '
		return string[:length]
	else:
		return string[:length]

def save_log(row):



	with open(log_file + now_date + '.log', '+a') as f:
		f.write(json.dumps(row) + '\n')



def get_host(ip):
	# RESOLVES IPS TO DNS NAMES
	# LOWERS NETWORK FOOTPRINT BY USING LOCAL TABLE
	# OF PREVIOUSLY RESOLVED IPS

	response = ('NF', 0)
	if ip not in dns_resolved.keys():
		try:
			response = socket.gethostbyaddr(ip)

			dns_resolved.update({ip:response[0]})

			return response[0]
		except:

			try:

				response = get_whois(ip)
				dns_resolved.update({ip:response})

				return response
			except:
				return 'NF'
	else:

		return dns_resolved[ip]

def print_connection(con):
    print( \
            adjust_string(con['name'], 15),                                    \
            adjust_string((con['local_addr'] + ":" + con['local_port']), 21),  \
            adjust_string((con['remote_addr'] + ":" + con['remote_port']), 21),\
            adjust_string(con['host'], 30),                                    \
            adjust_string(con['status'], 5),                                   \
            adjust_string(str(con['pid']), 5),                                 \
            adjust_string(con['user'], 15) ,                                   \
            adjust_string(str(con['files_open']), 5)                           \
    )

def get_whois(ip):

	data = subprocess.check_output(["C:\\Cygwin\\bin\\curl.exe", str("https://www.whois.com/whois/" + ip ), '--silent' ])

	lines = str(data).split(' ')
	abemail = []
	for line in lines:

		if "@" in line and len(line) > 3:

			domain_tmp = line.split('@')
			domain = domain_tmp[1].split("\\")
			abemail.append(domain[0])


	return "[N] " + str(abemail[0])

def live():

	# variables
	all_connections = []

	# GET CONNECTION INFO AFTER SLEEP TIME
	sleep = 5

	while True:

		# GET CONNECTIONS
		cons = psutil.net_connections("inet4")

		# GET DETAILS ABOUT CONNECTIONS
		for con in cons:
			local_addr      = "-"
			local_port      = "-"
			remote_addr     = "-"
			remote_port     = "-"
			status          = con[5]
			pid             = con[6]
			process         = ''
			name            = ''
			cmdline         = ''
			files_open      = ''
			user            = ''

			# REQUIRED TO GENTLY FAIL WHEN THE PROCESSES ENDS WHILE SCRIPT IN LOOP
			try:
				process         = psutil.Process(pid)
				name            = process.name()
				cmdline         = process.cmdline()
				files_open      = len(process.open_files())
				user            = process.username()
			except:
				pass


			# CONDITIONALLY GET ADDRESSING
			if len(con[3]) > 1:
				local_addr = con[3][0]
				local_port = str(con[3][1])
			if len(con[4]) > 1:
				remote_addr = con[4][0]
				remote_port = str(con[4][1])

			# COLLECT INFORMATION FOR NON LOCALHOST CONNECTIONS
			# SKIP WHOIS.COM CONNECTIONS
			if local_addr != "127.0.0.1" and remote_addr != "127.0.0.1" and remote_addr != "-" and remote_addr != '64.91.226.82':

				# CREATE CONNECTION OBJECT
				dict_con = {
							'name':name,
		                    'local_addr': local_addr,
		                    'remote_addr': remote_addr,
		                    'local_port': local_port,
		                    'remote_port': remote_port,
		                    'status': status,
		                    'pid': pid,
		                    'user': user,
		                    'files_open': files_open,
		                    'cmdline': cmdline,
		                    'host' : get_host(remote_addr),
							'timestamp' : str(int(time.time()))
		        }


				if len(all_connections) == 0:
					all_connections.append(dict_con)
					print_connection(dict_con)

					# Writing to log
					save_log(dict_con)
				else:

					exist = 0
					for con in all_connections[-300:] :
						if 	set([ con['pid'], con['status'], con['local_addr'], con['local_port'], con['remote_addr'], con['remote_port']]) \
							== set([ dict_con['pid'], dict_con['status'], dict_con['local_addr'], dict_con['local_port'], dict_con['remote_addr'], dict_con['remote_port']]):
							exist = 1

					if exist == 0:
						all_connections.append(dict_con)
						print_connection(dict_con)
						# Writing to log
						save_log(dict_con)

		time.sleep(sleep)


# data = subprocess.check_output(['curl.exe', 'https://www.whois.com/whois/205.223.230.253'])
# lines = str(data).split(' ')
# abemail = []
# for line in lines:
# 	if "@" in line and len(line) > 3:
# 		domain_tmp = line.split('@')
# 		domain = domain_tmp[1].split("\\")
# 		abemail.append(domain[0])
# 		print(domain[0])
#
# " ".join(abemail)












# PRINT HEADER
print( \
        adjust_string('Process Name', 15),                                    \
        adjust_string('Local Address', 21),                                   \
        adjust_string('Remote Address', 21),                                  \
        adjust_string('DNS Hostname', 30),                                    \
        adjust_string('Stat', 5),                                             \
        adjust_string('PID', 5),                                              \
        adjust_string('USER', 15) ,                                           \
        adjust_string('FOC', 5)                                               \
)

# RUN
live()










# SQLIte connection
#conn = sqlite3.connect('')
